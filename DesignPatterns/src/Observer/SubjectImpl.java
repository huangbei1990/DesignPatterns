package Observer;

import java.util.ArrayList;
import java.util.List;

//具体通知者类
public class SubjectImpl implements SubjectI{
	
	private List<ObserverI> ObList=new ArrayList<ObserverI>();

	@Override
	public void attach(ObserverI ob) {
		ObList.add(ob);	
	}

	@Override
	public void detach(ObserverI ob) {
		ObList.remove(ob);
	}

	@Override
	public void notifyObserver() {
		for(ObserverI ob:ObList){
			ob.update();
		}
	}

}
