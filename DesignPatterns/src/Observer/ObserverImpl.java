package Observer;

//具体观察者类
public class ObserverImpl implements ObserverI{

	private String tag;
	
	public ObserverImpl(String tag){
		this.tag=tag;
	}
	
	@Override
	public void update() {
		System.out.println(tag+"收到通知了");
	}

}
