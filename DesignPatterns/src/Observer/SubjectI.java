package Observer;

//通知者接口
public interface SubjectI {
	
	public void attach(ObserverI ob);//增加观察者
	
	public void detach(ObserverI ob);//减少观察者
	
	public void notifyObserver();//通知观察者

}
