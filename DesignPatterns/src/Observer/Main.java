package Observer;

public class Main {
	
	public static void main(String args[]){
		ObserverI ob1=new ObserverImpl("观察者1");
		ObserverI ob2=new ObserverImpl("观察者2");
		ObserverI ob3=new ObserverImpl("观察者3");
		
		SubjectI subject=new SubjectImpl();
		subject.attach(ob1);
		subject.attach(ob2);
		subject.attach(ob3);
		
		subject.notifyObserver();
	}

}
