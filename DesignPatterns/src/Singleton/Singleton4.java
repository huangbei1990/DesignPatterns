package Singleton;
//使用内部私有类按需创建
public class Singleton4 {
	
	private Singleton4(){}
	
	
	private static Singleton4 newInstance(){
		return single.instance;
	}
	
	private static class single{
		private static Singleton4 instance=new Singleton4();
	}

}
