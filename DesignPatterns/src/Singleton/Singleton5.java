package Singleton;
//���μ�������
public class Singleton5 {
	
	private static Singleton5 instance;
	
	private Singleton5(){
		
	}
	
	public static Singleton5 newInstance(){
		if(instance == null){
			synchronized(instance){
				if(instance == null){
					instance = new Singleton5();
				}
				return instance ;
			}
		}else{
		   return instance;
		}
	}

}
