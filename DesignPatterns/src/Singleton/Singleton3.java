package Singleton;

//懒汉，线程安全，效率不高
public class Singleton3 {

	private Singleton3(){}
	
	private static Singleton3 instance=null;
	
	public static synchronized Singleton3 newInstance(){
		if(instance==null){
			instance=new Singleton3();
		}
		return instance;
	}
}
