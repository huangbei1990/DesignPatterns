package Singleton;

//懒汉，线程不安全
public class Singleton1 {
	
	private Singleton1(){}
	
	private static Singleton1 instance=null;
	
	public static Singleton1 newInstance(){
		if(instance==null){
			instance=new Singleton1();
		}
		return instance;
	}

}
