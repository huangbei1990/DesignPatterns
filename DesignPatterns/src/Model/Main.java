package Model;

import Model.Car.BMW;
import Model.Car.Benz;
import Model.CarModel.CarModel;

public class Main {
	
	public static void main(String args[]){
		CarModel benz = new Benz();
		benz.run();
		
		CarModel bmw = new BMW();
		bmw.run();
	}

}
