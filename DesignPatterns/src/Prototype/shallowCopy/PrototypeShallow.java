package Prototype.shallowCopy;

import java.util.ArrayList;
import java.util.List;

//ǳ����
public class PrototypeShallow implements Cloneable{
	
	private ArrayList<String> typeList = new ArrayList<String>();
	
	public PrototypeShallow(){
//		this.typeList = new ArrayList<String>();
	}

	@Override
	public PrototypeShallow clone() {
		PrototypeShallow instance = null;
		try{
			instance = (PrototypeShallow)super.clone();
		}catch(Exception e){
			e.printStackTrace();
		}
		return instance;
	}
	
    public ArrayList<String> getTypeList(){
    	return this.typeList;
    }
	

    public String toString(){
    	StringBuilder builder = new StringBuilder();
    	for(String str : typeList){
    		builder.append(str);
    		builder.append("\n");
    	}
    	return builder.toString();
    }
}
