package Prototype;

import Prototype.deepCopy.PrototypeDeep;
import Prototype.shallowCopy.PrototypeShallow;

/**
 * 原型模式
 * @author A
 *
 */
public class Main {
	
	public static void main(String args[]){
		PrototypeDeep deep = new PrototypeDeep();
		PrototypeShallow shallow = new PrototypeShallow();
		deep.getTypeList().add("张三deep");
		shallow.getTypeList().add("张三shallow");
		
		PrototypeDeep cloneDeep = deep.clone();
		cloneDeep.getTypeList().add("李四deepclone");
		
		PrototypeShallow cloneShallow = shallow.clone();
		cloneShallow.getTypeList().add("李四shallowclone");
		
		System.out.println("deep#######################");
		System.out.println(deep.toString());
		System.out.println("cloneDeep##################");
		System.out.println(cloneDeep.toString());
		System.out.println("shallow##################");
		System.out.println(shallow.toString());
		System.out.println("cloneShallow##################");
		System.out.println(cloneShallow.toString());
	}

}
