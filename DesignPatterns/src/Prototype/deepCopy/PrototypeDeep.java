package Prototype.deepCopy;

import java.util.ArrayList;

 //���
public class PrototypeDeep implements Cloneable{
	
private ArrayList<String> typeList = new ArrayList<String>();
	
	public PrototypeDeep(){
		//this.typeList = new ArrayList<String>();
	}

	@Override
	public PrototypeDeep clone()  {
		PrototypeDeep instance = null;
		try{
			instance = (PrototypeDeep)super.clone();
			instance.typeList =  (ArrayList<String>)this.typeList.clone();
		}catch(Exception e){
			e.printStackTrace();
		}
		return instance;
	}
	
	public ArrayList<String> getTypeList(){
    	return this.typeList;
    }
	

    public String toString(){
    	StringBuilder builder = new StringBuilder();
    	for(String str : typeList){
    		builder.append(str);
    		builder.append("\n");
    	}
    	return builder.toString();
    }

}
