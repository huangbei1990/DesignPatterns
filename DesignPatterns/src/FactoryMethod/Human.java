package FactoryMethod;

public interface Human {
    public void talk();
    public void Color();
}
