package FactoryMethod;

public class HumanFactory extends AbstractHumanFactory{

	@Override
	public Human createHuman(Class c) {
		Human human=null;
		try{
			human=(Human)Class.forName(c.getName()).newInstance();
		}catch(Exception e){
			e.printStackTrace();
		};
		return human;
	}

}
