package FactoryMethod;

public class Main {
	
	public static void main(String args[]){
		AbstractHumanFactory factory=new HumanFactory();
		Human whiteHuman=factory.createHuman(WhiteHuman.class);
		whiteHuman.talk();
		whiteHuman.Color();
		Human blackHuman=factory.createHuman(BlackHuman.class);
		blackHuman.talk();
		blackHuman.Color();
		Human yellowHuman=factory.createHuman(YellowHuman.class);
		yellowHuman.talk();
		yellowHuman.Color();
	}

}
