package FactoryMethod;

public abstract class AbstractHumanFactory {
	
      public abstract  Human createHuman(Class c);
}
