package AbstractFactoryMethod.human;

public interface Human {
	
	public String getColor();
	
	public void talk();
	
    public String getSex();
    
}
