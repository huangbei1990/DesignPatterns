package AbstractFactoryMethod.human.abstractHuman;

import AbstractFactoryMethod.human.Human;

public abstract class WhiteHuman implements Human{

	@Override
	public String getColor() {
		return "��ɫ";
	}

	@Override
	public void talk() {
		System.out.println("˵Ӣ�");
	}

	@Override
	public abstract String getSex();
    
}
