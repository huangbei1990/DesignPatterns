package AbstractFactoryMethod.human.abstractHuman;

import AbstractFactoryMethod.human.Human;

public abstract class YellowHuman implements Human {

	@Override
	public String getColor() {
		return "��ɫ";
	}

	@Override
	public void talk() {
		System.out.println("˵���");
	}

	@Override
	public abstract String getSex();

}
