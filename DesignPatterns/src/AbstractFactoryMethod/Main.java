package AbstractFactoryMethod;

import AbstractFactoryMethod.factory.AbstractHumanFactory;
import AbstractFactoryMethod.factory.ManFactory;
import AbstractFactoryMethod.factory.WomanFactory;
import AbstractFactoryMethod.human.Human;

public class Main {
	
	public static void main(String args[]){
		//女生工厂
		AbstractHumanFactory womanFactory = new WomanFactory();
		//创造黑人女生
		Human human1 = womanFactory.createHuman("黑人");
		System.out.println(human1.getColor());
		System.out.println(human1.getSex());
		human1.talk();
		
		//创造白人女生
		Human human2 = womanFactory.createHuman("白人");
		System.out.println(human2.getColor());
		System.out.println(human2.getSex());
		human2.talk();
		
		//男生工厂
		AbstractHumanFactory manFactory = new ManFactory();
		//创造黄种人男生
		Human human3 = manFactory.createHuman("黄种人");
		System.out.println(human3.getColor());
		System.out.println(human3.getSex());
		human3.talk();
		
		//创造白人男生
		Human human4 = manFactory.createHuman("白人");
		System.out.println(human4.getColor());
		System.out.println(human4.getSex());
		human4.talk();
	}

}
