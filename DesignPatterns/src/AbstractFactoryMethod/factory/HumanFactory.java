package AbstractFactoryMethod.factory;

import AbstractFactoryMethod.human.Human;

//确定人种的颜色
public abstract class HumanFactory implements AbstractHumanFactory {

	@Override
	public Human createHuman(String hm) {
		Human human = null;
		  switch(hm){
		    case "白人":
		      human = createWhiteHuman();
		      break ;
		    case "黑人":
		      human = createBlackHuman();
		      break ;
		    case "黄种人":
		      human = createYellowHuman();
		      break ;
		  }
		  return human;
	}
	
	public abstract Human createWhiteHuman();

	public abstract Human createBlackHuman();
	
	public abstract Human createYellowHuman();
}
