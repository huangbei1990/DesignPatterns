package AbstractFactoryMethod.factory;

import AbstractFactoryMethod.human.Human;
import AbstractFactoryMethod.human.concerteHuman.BlackMan;
import AbstractFactoryMethod.human.concerteHuman.WhiteMan;
import AbstractFactoryMethod.human.concerteHuman.YellowMan;

//确定人种的性别
public class ManFactory extends HumanFactory{

	@Override
	public Human createWhiteHuman() {
		return new WhiteMan();
	}

	@Override
	public Human createBlackHuman() {
		return new BlackMan();
	}

	@Override
	public Human createYellowHuman() {
		return new YellowMan();
	}

}
