package AbstractFactoryMethod.factory;

import AbstractFactoryMethod.human.Human;
import AbstractFactoryMethod.human.concerteHuman.BlackWoman;
import AbstractFactoryMethod.human.concerteHuman.WhiteWoman;
import AbstractFactoryMethod.human.concerteHuman.YellowWoman;

//确定人种的性别
public class WomanFactory extends HumanFactory {

	@Override
	public Human createWhiteHuman() {
		return new WhiteWoman();
	}

	@Override
	public Human createBlackHuman() {
		return new BlackWoman();
	}

	@Override
	public Human createYellowHuman() {
		return new YellowWoman();
	}

}
