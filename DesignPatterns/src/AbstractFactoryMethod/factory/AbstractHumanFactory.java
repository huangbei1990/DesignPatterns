package AbstractFactoryMethod.factory;

import AbstractFactoryMethod.human.Human;

public interface AbstractHumanFactory {
	
	public Human createHuman(String hm);

}
